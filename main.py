import base64
import io
import json
import os
from typing import Optional, List
from urllib.parse import parse_qsl, urlencode, urlsplit, urlunsplit

from PIL import Image
from fastapi import FastAPI
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.webdriver import WebDriver
from starlette.responses import StreamingResponse

app = FastAPI()

command_executor = os.getenv('SELENIUM_COMMAND_EXECUTOR')

DEFAULT_CHROME_OPTIONS = [
    '--disable-gpu',
    '--no-sandbox',
    '--no-zygote',
    '--disable-features=TranslateUI',
    '--disable-extensions',
    '--disable-component-extensions-with-background-pages',
    '--disable-background-networking',
    '--disable-sync',
    '--metrics-recording-only',
    '--disable-default-apps',
    '--mute-audio',
    '--no-default-browser-check',
    '--no-first-run',
    '--disable-backgrounding-occluded-windows',
    '--disable-renderer-backgrounding',
    '--disable-background-timer-throttling',
    '--force-fieldtrials=*BackgroundTracing/default/',
    # '--remote-debugging-port=44333',
    '--disable-setuid-sandbox',
    # '--user-data-dir=/home/seluser/user-data',
    # '--profile-directory=Default',
    # '--remote-debugging-address=0.0.0.0',
]


def get_chrome_options(arguments: List[str] = None) -> Options:
    if arguments is None:
        arguments = []
    options = webdriver.ChromeOptions()

    arguments += DEFAULT_CHROME_OPTIONS
    for arg in arguments:
        options.add_argument(arg)
    return options


def hide_scrollbars(driver: WebDriver):
    inject_css_hide_scrollbars = """
        const addStyle = (() => {
            const style = document.createElement('style');
            document.head.append(style);
            return (styleString) => style.textContent = styleString;
        })();

        const hide_scrollbars = `
            ::-webkit-scrollbar {
                display: none;
            }

            html > ::-webkit-scrollbar {
                width: 0px;
                display: none;
            }

            ::-webkit-scrollbar-thumb {
                display: none;
            }

            ::-webkit-scrollbar-track-piece {
                display: none;
            }
        `

        const callback = arguments[0];
        const handleDocumentLoaded = () => {
            addStyle(hide_scrollbars);
            callback();
        }

        if (document.readyState === "loading") {
            document.addEventListener("DOMContentLoaded", handleDocumentLoaded);
        } else {
            handleDocumentLoaded();
        }
    """
    driver.execute_async_script(inject_css_hide_scrollbars)


def send_devtools(driver: WebDriver, cmd: str, params: Optional[dict] = None) -> dict:
    if params is None:
        params = dict()

    resource = "/session/%s/chromium/send_command_and_get_result" % driver.session_id
    url = driver.command_executor._url + resource
    body = json.dumps({'cmd': cmd, 'params': params})

    response = driver.command_executor._request('POST', url, body)
    if response.get('status'):
        raise Exception(response.get('value'))
    return response.get('value')


@app.get("/pdf/{url:path}")
def pdf(url: str, partner_id: Optional[str] = None, locale: Optional[str] = None):
    capabilities = webdriver.DesiredCapabilities.CHROME.copy()
    chrome_options = get_chrome_options([
        '--headless'
    ])

    driver = webdriver.Remote(command_executor=command_executor,
                              desired_capabilities=capabilities,
                              options=chrome_options)

    if partner_id or locale:
        url = add_partner_id_and_locale_in_url(url, partner_id, locale)

    driver.get(url)

    calculated_print_options = {
        'landscape': False,
        'displayHeaderFooter': False,
        'printBackground': True,
        'preferCSSPageSize': True,
    }
    result = send_devtools(driver, "Page.printToPDF", calculated_print_options)

    driver.close()

    pdf = base64.b64decode(result['data'])
    return StreamingResponse(io.BytesIO(pdf), media_type="application/pdf")


@app.get("/screenshot/{url:path}")
def screenshot(url: str, width: int = 1280, height: int = 800, partner_id: Optional[str] = None,
               locale: Optional[str] = None):
    capabilities = webdriver.DesiredCapabilities.CHROME.copy()
    chrome_options = get_chrome_options([
        '--high-dpi-support=1',
        '--hide-scrollbars'
    ])
    driver = webdriver.Remote(command_executor=command_executor,
                              desired_capabilities=capabilities,
                              options=chrome_options)

    # set viewport as request image size
    width += 10  # side bar
    height += 82  # top bar

    driver.set_window_size(width, height)

    if partner_id or locale:
        url = add_partner_id_and_locale_in_url(url, partner_id, locale)

    driver.get(url)

    hide_scrollbars(driver)

    image = driver.get_screenshot_as_png()

    image = Image.open(io.BytesIO(image))
    image = image.convert('RGB')    # avoid transparency from png
    with io.BytesIO() as f:
        image.save(f, format='jpeg', quality=60)
        image = f.getvalue()

    driver.close()
    return StreamingResponse(io.BytesIO(image), media_type="image/jpeg")


def add_partner_id_and_locale_in_url(url: str, partner_id: Optional[str] = None, locale: Optional[str] = None):
    scheme, netloc, path, query, fragment = urlsplit(url)
    parsed_query = parse_qsl(query)
    if partner_id:
        parsed_query.append(('partner_id', partner_id))
    if locale:
        parsed_query.append(('locale', locale))
    query = urlencode(parsed_query)
    url = urlunsplit((scheme, netloc, path, query, fragment))
    return url
