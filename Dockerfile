FROM python:3

WORKDIR /usr/src/app
ENV PYTHONPATH=.

COPY requirements.txt ./
# fix ModuleNotFoundError: No module named 'pip.download'
RUN python -m pip install pip==19.1.1
RUN pip3 install --no-cache-dir -r requirements.txt

COPY . .

CMD uvicorn main:app --reload --host 0.0.0.0